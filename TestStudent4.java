package Secondprg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;

public class TestStudent4 
{

public static void main(String[] args)
{ 
ArrayList <Student4> set = new ArrayList();
set.add(new Student4(576,"swarna",90));
set.add(new Student4(533,"satheesh",99));
set.add(new Student4(213,"sudha",45));
Collections.sort(set);
System.out.println("name wise sorting");
Iterator itr=set.iterator();
while(itr.hasNext()){
Student4 st=(Student4)itr.next();
System.out.println(st.rollno+" "+st.name+" "+st.prc);
}
Collections.sort(set,new PercComparator());
System.out.println("Percentage wise sorting");
Iterator itr1=set.iterator();
while(itr1.hasNext()){
Student4 st=(Student4)itr1.next();
System.out.println(st.rollno+" "+st.name+" "+st.prc);
}
}
}


	