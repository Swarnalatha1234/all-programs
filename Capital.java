package Secondprg;

public class Capital{
	
		
		private static String stringName;
		private int numOfUpperCase(String str)
		{
			int UpperCase=0;
			for(int i=0;i<str.length();i++)
			{
				char currchar=str.charAt(i);
				if(currchar>=65 && currchar<=91)
				{
					UpperCase++;
				}
			}
			return UpperCase;
		}
		private int numOfLowerCase(String str)
		{
			int LowerCase=0;
			for(int i=0;i<str.length();i++)
			{
				char currchar=str.charAt(i);
				if(currchar>=92 && currchar<=122)
				{
					LowerCase++;
				}
			}
			return LowerCase;
		}
		private int numOfDigits(String str)
		{
			int Digits=0;
			for(int i=0;i<str.length();i++)
			{
				char currchar=str.charAt(i);
				if(currchar>='0' && currchar<='9')
				{
					Digits++;
				}
			}
			return Digits;
		}
		private int numOfSpecialchar(String str)
		{
			int Specialchar=0;
			for(int i=0;i<str.length();i++)
			{
				char currchar=str.charAt(i);
				if(currchar=='!'||currchar=='@'||currchar=='#'||currchar=='&')
				{
					Specialchar++;
				}
			}
			return Specialchar;
		}
		
		public static void main(String args[])
		{
			String str="This is Chennai 1 & That is Airport 20 ";
			Capital c=new Capital();
			System.out.println("UpperCase characters"+" "+c.numOfUpperCase(str));
			System.out.println("LowerCase characters"+" "+c.numOfLowerCase(str));
			System.out.println("Digits is"+" "+c.numOfDigits(str));
			System.out.println("Special characters"+" "+c.numOfSpecialchar(str));
		}	
		
		
		
}
	
	